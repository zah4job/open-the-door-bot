FROM python:3.10-buster
RUN mkdir "/app"
COPY . /app
WORKDIR /app
RUN pip install -U pip
RUN pip install poetry==1.1.14
RUN poetry install
CMD poetry run python ./open_the_door_bot