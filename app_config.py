from environs import Env

env = Env()
env.read_env('./config/.env')
token = env.str('ODO_TOKEN', '')
