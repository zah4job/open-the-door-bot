from dataclasses import dataclass
from typing import Set, Dict

steps = {
    1: 'Подёргать ручку',
    2: 'Потянуть шнурок',
    3: 'Лизнуть рюшку',
    4: 'Потрясти тютьку',
    5: 'Поцарапать дверь',
    6: 'Крикнуть',
    7: 'Почесать чесалку',
    8: 'Покрутить крутилку',
    9: 'Потеребонькать теребоньку',
    10: 'Перекреститься',
    11: 'Выдохнуть',
    12: 'Вдохнуть',
    13: 'Пнуть ногой',
}

right_way = [6, 4, 3, 1, 11, 12, 10, 8, 5, 2, 7, 9, 13]


@dataclass
class TurnResult(object):
    """Результаты хода."""
    fail: bool
    message: str
    game_over: bool
    available_steps: Dict[int, str]


def do_turn(step_set: Set[int], new_step: int):
    target_step = right_way[len(step_set)]
    if new_step != target_step:
        return TurnResult(
            fail=True,
            message='Ты умер',
            game_over=True,
            available_steps={},
        )
    step_set = set([new_step]).union(step_set)

    available_steps = {
        step_key: step_text
        for step_key, step_text in steps.items()
        if step_key not in step_set
    }
    victory = not available_steps
    if victory:
        return TurnResult(
            fail=False,
            message='Ты победил',
            game_over=True,
            available_steps=available_steps,
        )
    return TurnResult(
        fail=False,
        message='Сделай следующий ход',
        available_steps=available_steps,
        game_over=False,
    )