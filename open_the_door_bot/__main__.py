import json
from typing import List, Tuple

from loguru import logger
from telegram import Update, InlineKeyboardMarkup, InlineKeyboardButton

import app_config
from open_the_door_bot import gameplay
from open_the_door_bot.gameplay import do_turn
from telegram.ext import Updater, CallbackContext, CommandHandler, CallbackQueryHandler


def console_ui():
    play = True
    done_steps = {}
    start_steps = gameplay.steps.copy()
    logger.info('выбери вариант:')
    for step_key, step_name in start_steps.items():
        logger.info(f'{step_key}: {step_name}')
    while play:
        user_step = input()
        turn_result = do_turn(done_steps, user_step)
        logger.info(turn_result)


def telegram_ui():
    updater = Updater(token=app_config.token, use_context=True)
    dispatcher = updater.dispatcher
    start_handler = CommandHandler('start', start)
    dispatcher.add_handler(start_handler)

    btn_handler = CallbackQueryHandler(callback_query_handler)
    dispatcher.add_handler(btn_handler)

    updater.start_polling()


def build_buttons(done_steps, available_steps):
    done_steps = list(done_steps)
    buttons = [
        InlineKeyboardButton(
            text=f'{step_key}: {step_text}',
            callback_data=pack(done_steps, step_key),
        )
        for step_key, step_text in available_steps.items()
    ]
    layout_buttons = []
    for button in buttons:
        layout_buttons.append([button])
    markup = InlineKeyboardMarkup(inline_keyboard=layout_buttons)
    return markup


def start(update: Update, context: CallbackContext):
    # context.bot.send_message(
    #     chat_id=update.effective_chat.id,
    #     text="I'm a bot, please talk to me!",
    # )
    context.bot.send_message(
        text='Перед тобой закрытая дверь. Что сделать?',
        chat_id=update.effective_chat.id,
        reply_markup=build_buttons({}, gameplay.steps),
    )


def callback_query_handler(update: Update, context: CallbackContext):
    done_steps, new_turn = unpack(update.callback_query.data)
    logger.info(f'{update.effective_user}')
    logger.info(f'{done_steps=}')
    logger.info(f'{new_turn=}')
    turn_result = gameplay.do_turn(done_steps, new_turn)

    message = update.callback_query.message
    if turn_result.fail:
        message.edit_text('Ты погиб. Неправильный выбор.')
    elif turn_result.game_over:
        message.edit_text('Ты победил в этой игре! МОЛОДЧУЛЯ')
    else:
        # можно делать следующий ход
        done_steps.append(new_turn)
        next_turns = turn_result.available_steps
        logger.info('Следующий ход')
        done_steps_str = ', '.join([
            gameplay.steps[step_key]
            for step_key in done_steps
        ])
        msg = f'Отлично. Успешно пройдено: {done_steps_str}.'
        message.edit_text(
            text=f'{msg} Какой будет следующий шаг?',
            reply_markup=build_buttons(done_steps, next_turns),
        )


def pack(done_steps: List[int], new_turn=int) -> str:
    steps_str = [str(step) for step in done_steps]
    done_steps_str = ' '.join(steps_str)
    return f'{done_steps_str};{new_turn}'


def unpack(packed: str) -> Tuple[List[int], int]:
    done_steps_str, new_turn_str = packed.split(';')
    done_steps = [
        int(number) for number
        in done_steps_str.split(' ')
        if number
    ]
    return done_steps, int(new_turn_str)


if __name__ == '__main__':
    logger.info('Начало игры')
    telegram_ui()

